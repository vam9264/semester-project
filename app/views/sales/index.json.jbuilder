json.array!(@sales) do |sale|
  json.extract! sale, :id, :customer, :employee, :color, :model, :total_price, :interest_rate, :status, :date
  json.url sale_url(sale, format: :json)
end
