json.array!(@cars) do |car|
  json.extract! car, :id, :color, :make, :model, :vin_number, :msrp
  json.url car_url(car, format: :json)
end
