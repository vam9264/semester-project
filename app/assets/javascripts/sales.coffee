# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


build_table = ->
  time_frame =  $("#sale_time_frame").val()
  interest_rate = $("#sale_interest_rate").val()
  starting_amount = $("#sale_starting_amount").val()
  $.get "/sale/interest",{amount: starting_amount,time: time_frame, rate: interest_rate}

$ ->
  $('#sale_time_frame,#sale_interest_rate,#sale_starting_amount').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()


