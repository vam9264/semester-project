class DashboardController < ApplicationController

  def index
    @total_gross_revenue = 0
    @net_profit = 0
    @sales_tax_totals = 0
  end

end