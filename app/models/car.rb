class Car < ActiveRecord::Base
  has_many :sales

  def car
    "#{color} | #{make} | #{model} | #{vin_number} | #{msrp}"
  end

  def self.search(search)
    if search
      self.where("model like ? OR color like ? OR vin_number like ?", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end
end
