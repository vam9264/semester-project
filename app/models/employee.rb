class Employee < ActiveRecord::Base

  has_many :sales
end

def emp_name
  "#{f_name}, #{l_name} "
end