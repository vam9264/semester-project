class Sale < ActiveRecord::Base
  belongs_to :car
  belongs_to :customer
  belongs_to :employee

  def self.calculate_payment_table(total_price = 0, year = 0, interest = 0)
    Rails.logger.debug "Inputs: #{total_price} #{year} #{interest}"
    payment = []
    (0..year).each do |t|
      payment << (interest*total_price*(1+interest)**(year*12))/(((1+interest)**(year*12))-1)
    end
    return payment
  end

  def calculate_payment_table
    return self.class.calculate_payment_table(self.total_price,self.interest,self.year)
  end
end


