class Customer < ActiveRecord::Base
  has_many :sales

  def self.search(search)
    if search
      self.where("f_name like ?, OR l_name like ?"  "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end
end

def cus_name
  "#{f_name}, #{l_name}"
end