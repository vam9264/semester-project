class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :car_id
      t.decimal  :total_price
      t.decimal  :interest
      t.integer :year
      t.string :status
      t.date :date

      t.timestamps null: false
    end
  end
end
