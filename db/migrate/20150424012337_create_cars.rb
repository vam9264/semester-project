class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :color
      t.string :make
      t.string :model
      t.string :vin_number
      t.integer :msrp

      t.timestamps null: false
    end
  end
end
